Rails.application.routes.draw do

  root to: 'home#index'

  resources :home
  resources :tests do
    get 'close', on: :collection
  end
  resources :questions
  resources :users do
    get 'details', on: :collection
    get 'score', on: :member
  end
  resources :answers
end
