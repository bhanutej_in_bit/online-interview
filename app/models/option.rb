class Option
  include Mongoid::Document
  include Mongoid::Timestamps

  belongs_to :question
  belongs_to :answerer, class_name: 'Question', inverse_of: :answers

  field :opt_value, type: String
  validates_presence_of :opt_value
end
