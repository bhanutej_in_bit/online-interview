class Question
  include Mongoid::Document
  include Mongoid::Timestamps

  has_many :options
  has_many :answers, class_name: 'Option', inverse_of: :answerer

  field :content, type: String
  validates_presence_of :content

  field :option_type, type: String
  validates_presence_of :option_type

  field :seconds, type: Fixnum
  validates_presence_of :seconds
end
