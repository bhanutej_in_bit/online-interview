class Answer
  include Mongoid::Document
  include Mongoid::Timestamps

  belongs_to :question
  belongs_to :user

  field :answer_ids, type: Array
  validates_presence_of :answer_ids
  field :time_taken, type: Fixnum
end
