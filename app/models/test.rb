class Test
  include Mongoid::Document
  include Mongoid::Timestamps

  embeds_many :questions
end
