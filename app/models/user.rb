class User
  include Mongoid::Document
  include Mongoid::Timestamps

  has_many :answers

  field :name, type: String
  validates_presence_of :name

  field :email, type: String, default: ''
  validates :email, presence: true, allow_blank: false
  validates :email, format: { with: /\A[a-zA-Z0-9.!#$%&'\*\+\/=\?\^\_\`\{\|\}\~-]{1,64}@((?:[-a-z0-9]+\.)+[a-z]{2,})\s*\z/i, message: "Please provide valid email" }
  validates_uniqueness_of    :email

  field :phone_number
  validates_presence_of :phone_number
end
