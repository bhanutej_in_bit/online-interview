class AnswersController < ApplicationController

  def create
    question_id = params[:question_id]
    answers = params[:answers]
    time_taken = params[:time_taken]
    question = Question.find(question_id)
    time_taken_for_question = time_taken_by_user(question, time_taken)
    answer = Answer.new
    answer.question = question
    answer.user_id = session[:current_user]['_id']['$oid']
    answer.answer_ids = answers.split(',') if answers.present?
    answer.time_taken = time_taken_for_question
    answer.save
  end

  private

  def time_taken_by_user(question, time_taken)
    seconds = question.seconds
    time_taken_mins = time_taken.split(':')[0]
    time_taken_secs = time_taken.split(':')[1]
    total_secs = (time_taken_mins.to_i*60) + time_taken_secs.to_i
    seconds - total_secs
  end
end
