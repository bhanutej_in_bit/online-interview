class UsersController < ApplicationController
  include Scores

  def index

  end

  def create
    name = params[:name]
    email = params[:email]
    phone_number = params[:phone_number]
    @user = User.create(name: name, email: email, phone_number: phone_number)
    if @user.valid?
      session[:current_user] = @user
    end
  end

  def details
    @users = User.all
    session.delete(:current_user)
  end

  def score
    user_id = params[:id]
    @user, @questions, @correct_ans, @user_attempted_questions = get_scores(user_id)
  end

end
