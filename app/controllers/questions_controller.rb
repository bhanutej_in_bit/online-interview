class QuestionsController < ApplicationController
  include Questions

  def index
    if session[:current_user].blank?
      redirect_to users_path
    else
      qns = Option.where(:answerer_id.ne => nil).pluck(:question_id).uniq
      @questions = Question.where(id: { '$in': qns }).order_by(created_at: 'desc')
    end
  end

  def create
    question_content = params[:question_content]
    option_type = params[:option_type]
    minutes = params[:minutes].to_i.minutes
    option_values = params[:option_value]
    @errors, @questions = create_question(minutes, option_type, option_values, question_content)
  end
end
