class TestsController < ApplicationController
  include Scores


  def index
    qns = Option.where(:answerer_id.ne => nil).pluck(:question_id).uniq
    @questions = Question.where(id: { '$in': qns }).order_by(created_at: 'desc')
    session.delete(:current_user)
  end

  def close
    user_id = session[:current_user]['_id']['$oid']
    @user, @questions, @correct_ans, @user_attempted_questions = get_scores(user_id)
    session.delete(:current_user)
  end
end
