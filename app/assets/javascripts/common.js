$(function(){
    // flash toast message OPTIONS
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "progressBar": true,
        "positionClass": "toast-bottom-right",
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "2000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };
    flashMessages();
    // Pagination
    $("body").on("click",".show-pagination-loader a",function(){
        container = $(this).closest('.pagination_container');
        showPaginationLoader(container);
    });
});

function flashMessages(){
    if($(".flash-messages-container").find("div").length)
    {
        var messages_obj = $(".flash-messages-container").find("div");

        $.each( messages_obj, function( key, obj ) {
            var msgType = $(obj).attr("class"),
                content = $(obj).html();

            showFlashMessage(msgType, content);

        });
    }
}

function showFlashMessage(msgType,message){
    switch(msgType)
    {
        case "error":
            toastr.options.timeOut = 600000;
            toastr.options.extendedTimeOut = 150000;
            toastr.options.progressBar = false;
            toastr["error"](message, "Error");
            toastr.options.timeOut = 5000;
            toastr.options.extendedTimeOut = 2000;
            toastr.options.progressBar = true;
            break;
        case "explicit_success":

            toastr.options = {
                "closeButton": true,
                "tapToDismiss": false,
                "positionClass": "toast-top-right"
            };

            toastr.options.timeOut = 600000;
            toastr.options.extendedTimeOut = 150000;
            toastr.options.progressBar = false;
            toastr["success"](message, "");
            toastr.options.timeOut = 5000;
            toastr.options.extendedTimeOut = 2000;
            toastr.options.progressBar = true;

            break;
        case "success":
            toastr["success"](message, "Success");
            break;
        case "info":
            toastr["info"](message, "Info");
            break;
        case "notice":
            toastr["success"](message, "Info");
            break;
        case "warning":
            toastr["warning"](message, "Warning");
            break;
        case "alert":
            toastr["warning"](message, "Warning");
            break;
    }
}

function showModel(title, modal_type){
    modal_type = modal_type || false;
    $("#generic_modal").find(".modal-dialog").removeClass("modal-lg");
    if(modal_type)
    {
        $("#generic_modal").find(".modal-dialog").addClass("modal-lg");
    }
    $("#generic_modal .modal-title").html(title);
    $("#generic_modal").modal('show');
}

function hideModel(){
    $("#generic_modal").modal('hide');
}
