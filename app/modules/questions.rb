module Questions

  def create_question(minutes, option_type, option_values, question_content)
    errors = []
    questions = nil
    any_ans = option_values.map { |option| option[:is_answer].present? }
    any_ans = any_ans.include? true
    errors = ['Select one of option as answer'] unless any_ans
    question = Question.new(content: question_content, option_type: option_type, seconds: minutes)
    errors = ['Provide options for question'] if option_values.length == 1 && option_values[0][:op_value].blank?
    if question.save
      if errors.blank?
        option_values.each do |optn_value|
          if optn_value[:op_value].present?
            optn = question.options.create(opt_value: optn_value[:op_value])
            question.answers << optn if optn_value[:is_answer].present? && optn_value[:is_answer] == 'true'
            question.save
          end
        end
      end
      qns = Option.where(:answerer_id.ne => nil).pluck(:question_id).uniq
      questions = Question.where(id: {'$in': qns}).order_by(created_at: 'desc')
    else
      errors = question.errors.full_messages
    end
    [errors, questions]
  end

end
