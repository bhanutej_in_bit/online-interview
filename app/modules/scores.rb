module Scores

  def get_scores(user_id)
    user = User.find(user_id)
    qns = Option.where(:answerer_id.ne => nil).pluck(:question_id).uniq
    questions = Question.where(id: {'$in': qns}).order_by(created_at: 'desc')
    correct_ans = 0
    user_attempted_questions = Answer.where(user_id: user_id)
    questions.each do |question|
      user_attempted_questions.each do |answer|
        if question.id == answer.question_id
          qns_ans = question.answer_ids.map { |x| x.to_s }
          user_ans = answer.answer_ids
          ans = user_ans.map { |ans_id| qns_ans.include? ans_id }
          correct_ans += 1 if ans.all? { |ele| ele }
        end
      end
    end
    [user, questions, correct_ans, user_attempted_questions]
  end

end
